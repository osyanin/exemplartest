package exemplar;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args) throws JSONException, IOException {

        final File f = new File("src/resources/json.json");
        final URL url = new URL("http://136.243.106.35:8001/buyer/between");

        byte[] encoded = Files.readAllBytes(Paths.get(f.getAbsolutePath()));
        final String Phew = new String(encoded, StandardCharsets.UTF_8);

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost postRequest = new HttpPost(String.valueOf(url));

        JSONObject objFromFile = new JSONObject(Phew);
        JSONObject deviceJSONObj = new JSONObject();
        JSONObject geoJSONObj = new JSONObject();

        final String geoJsonCity = "Moscow";
        final String geoJsonCountry = "RU";
        final String geoJsonRegion = "48";
        final String geoJsonZip = "";

        final String deviceJsonFlashver = "16";
        final String deviceJsonLanguage = "ru";

        final String browserHeaders = "Mozilla/5.0 (Android; Linux armv7l; rv:9.0) Gecko/20111216 Firefox/9.0 Fennec/9.0";
        final String ips = "176.59.64.0";
        //31.173.84.160 megafon ru
        //213.87.131.228 mts ru
        //83.220.236.237 beeline ru
        //83.220.237.31 beeline ru
        //5.134.220.110 office ru
        //176.59.64.0-176.59.255.255 tele2 ru
        //217.168.176.0-217.168.191.255 bakcell az 217.168.176.15

        //String[] ips = fileRead("src/resources/ips.txt");
        //String[] browserHeaders = fileRead("src/resources/headers.txt");

        objFromFile.put("device", deviceJSONObj);

        geoJSONObj.put("city", geoJsonCity);
        geoJSONObj.put("country", geoJsonCountry);
        geoJSONObj.put("region", geoJsonRegion);
        geoJSONObj.put("zip", geoJsonZip);

        deviceJSONObj.put("flashver", deviceJsonFlashver);
        deviceJSONObj.put("geo", geoJSONObj);
        deviceJSONObj.put("ip", ips);
        deviceJSONObj.put("language", deviceJsonLanguage);
        deviceJSONObj.put("ua", browserHeaders);

        StringEntity input = new StringEntity(objFromFile.toString());
        input.setContentType("application/json;charset=UTF-8");
        postRequest.setEntity(input);

        System.out.println(objFromFile.toString());

        try (CloseableHttpResponse response = httpClient.execute(postRequest)) {

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                //System.out.println(entity);
                try {
                    JSONObject JSONResponse = new JSONObject(EntityUtils.toString(response.getEntity()));
                    System.out.println(JSONResponse);
                    System.out.println(JSONResponse.getJSONArray("seatbid").getJSONObject(0).getJSONArray("bid").getJSONObject(0).getString("nurl"));
                    String nurl = JSONResponse.getJSONArray("seatbid").getJSONObject(0).getJSONArray("bid").getJSONObject(0).getString("nurl");
                    //nurl = nurl.substring()
                    System.out.println(JSONResponse.getJSONArray("seatbid").getJSONObject(0).getJSONArray("bid").getJSONObject(0).getString("adm"));
                    System.out.println(url);
                } finally {
                    response.close();
                }
            } else {
                System.out.println(response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        httpClient.close();
    }
}